﻿<?php 
$open="product";
  require_once __DIR__."/../../autoload/autoload.php" ;
  if(isset($_GET['page']))
  {
    $p=$_GET['page'];
  }
  else
  {
    $p=1;
  }
  $sql = "SELECT product.*,category.name as namecate FROM product LEFT JOIN category on category.id = product.category_id";

  $product = $db->fetchJone('product',$sql,$p,4,true);

  if(isset($product['page']))
  {
    $sotrang = $product['page'];
    unset($product['page']);
  }
?>
<?php require_once __DIR__. "/../../layouts/header.php";?>
  <div id="content-wrapper">

      <div class="container-fluid">
     <!-- Breadcrumbs-->
<div class="row">
  <div class="col-lg-12"> 
    <h1 class="page-header">
       Sản phẩm
    </h1>
    <ol class="breadcrumb">
      <li>
        <i class="fa fa-dashboad"> </i> <a href="index.html">Dashboad</a>
      </li>
      <li class="active">
        <i class="fa fa-file"></i> Sản phẩm
      </li>
    </ol>
     <div class="clearfix"></div>
    <?php require_once __DIR__. "/../../../partials/notification.php";?>
    <a href="add.php" class="btn btn-success">THÊM MỚI SẢN PHẨM</a>
   
<div class="row">
      <div class="col-lg-12">
        <div class="table-responsive">
  <table class="table table-bordered table-hover">
    <thead>
      <tr>
        <th>STT</th>
        <th>Tên</th>
        <th>Danh mục</th>
        <th>Hình</th>
        <th>Tên QT</th>
        <th>Nội dung</th>
        <th>Chỉnh sửa</th>
      </tr>
    </thead>
    <tbody>
      <?php $stt=1; foreach ($product as $item): ?>
          <tr>
            <td><?php echo $stt ?></td>
            <td><?php echo $item['name'] ?></td>
            <td><?php echo $item['namecate'] ?></td>
            <td>
              <img src="<?php echo uploads() ?>product/<?php echo $item['thunbar'] ?>" width="120px" height="120px">
            </td>
            <td><?php echo $item['slug'] ?></td>
            
            <td>
              <ul>
                <li> Giá : <?php echo $item['price'] ?></li>
                <li> Số lượng : <?php echo $item['number'] ?></li>
              </ul>
            </td>
            <td>
                <a class="btn btn-xs btn-info" href="edit.php?id=<?php echo $item['id'] ?>"<i class="fa fa-edit"></i>Sửa</a>
                <a class="btn btn-xs btn-danger" href="delete.php?id=<?php echo $item['id'] ?>"<i class="fa fa-times"></i>Xóa</a>
            </td>
          </tr>
    <?php $stt++ ;endforeach ?>   
    </tbody>
  </table> 
    <div class="pull-right"></div>
    <nav aria-label="page navigation" class="page-link">
      <ul class="pagination">
        <li>
          <a href=""  aria-label="previous">
            <span aria-hidden="true">&laquo;</span>
          </a>
        </li>
        
        <?php for(  $i=1;$i<=$sotrang;$i++) : ?>
          <?php 
            if(isset($_GET['page']))
            {
              $p=$_GET['page'];
            }
            else
            {
              $p=1;
            }
           ?>
           <li class="<?php echo($i==$p) ? 'active' : '' ?>">
                <a class="page-link" href="?page=<?php echo $i;?>"><?php echo $i; ?></a>
           </li>
         <?php endfor; ?>
         <li>
           <a href="" aria-label="Next">
             <span aria-hidden="true">&raquo;</span>
           </a>
         </li>
      </ul>
    </nav>
</div>  

<!-- <div class="dataTables_paginate paging_simple_numbers" id="dataTable_paginate">
    <ul class="pagination">
        <li class="paginate_button page-item active"><a href="#" aria-controls="dataTable" data-dt-idx="1" tabindex="0" class="page-link">1</a></li>
        <li class="paginate_button page-item "><a href="#" aria-controls="dataTable" data-dt-idx="2" tabindex="0" class="page-link">2</a></li>
        <li class="paginate_button page-item "><a href="#" aria-controls="dataTable" data-dt-idx="3" tabindex="0" class="page-link">3</a></li>
        <li class="paginate_button page-item "><a href="#" aria-controls="dataTable" data-dt-idx="4" tabindex="0" class="page-link">4</a></li>
        <li class="paginate_button page-item "><a href="#" aria-controls="dataTable" data-dt-idx="5" tabindex="0" class="page-link">5</a></li>
        <li class="paginate_button page-item "><a href="#" aria-controls="dataTable" data-dt-idx="6" tabindex="0" class="page-link">6</a></li>
        <li class="paginate_button page-item next" id="dataTable_next"><a href="#" aria-controls="dataTable" data-dt-idx="7" tabindex="0" class="page-link">Next</a></li>
    </ul>
</div> -->
  </div>
 
      </div>
      
  </div>
</div>


    <!-- /.content-wrapper -->



  <!-- /#wrapper -->
   <?php require_once __DIR__. "/../../layouts/header.php";?>