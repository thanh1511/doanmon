﻿<?php 
$open="transaction";
  require_once __DIR__."/../../autoload/autoload.php" ;
  unset($is_check);
  if(isset($_GET['page']))
  {
    $p=$_GET['page'];
  }
  else
  {
    $p=1;
  }
  $sql="SELECT transaction.* , users.name as nameuser , users.phone as phoneuser FROM transaction LEFT JOIN users ON users.id = transaction.users_id ORDER BY ID DESC";

  $transaction= $db->fetchJone("transaction",$sql,$p,10,true);
  if(isset($transaction['page']))
  {
    $sotrang = $transaction['page'];
    unset($transaction['page']);
  }

?>
<?php require_once __DIR__. "/../../layouts/header.php";?>
  <div id="content-wrapper">

      <div class="container-fluid">
     <!-- Breadcrumbs-->
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
      Danh sách đơn hàng
    </h1>
    <ol class="breadcrumb">
      <li>
        <i class="fa fa-dashboad"> </i> <a href="index.html">Dashboad</a>
      </li>
      <li class="active">
        <i class="fa fa-file"></i> Admin
      </li>
    </ol>
     <div class="clearfix"></div>
    <?php require_once __DIR__. "/../../../partials/notification.php";?>
   
<div class="row">
      <div class="col-lg-12">
        <div class="table-responsive">
  <table class="table table-bordered table-hover">
    <thead>
      <tr>
        <th>STT</th>
        <th>Tên</th>
        <th>Điện thoại</th>
        <th>Trạng thái</th>
        <th>Chỉnh sữa</th>
      </tr>
    </thead>
    <tbody>
      <?php $stt=1; foreach ($transaction as $item): ?>
          <tr>
            <td><?php echo $stt ?></td>
            <td><?php echo $item['nameuser'] ?></td>
            <td><?php echo $item['phoneuser'] ?></td>
            <td>
              <a href="status.php?id=<?php echo $item['id'] ?>" class="btn btn-xs <?php echo $item['status'] == 0 ? 'btn-danger' :'btn-info' ?>" > <?php echo $item['status'] == 0 ?'Chưa xử lý' : 'Đã xử lý' ?></a>
                   
            </td>
            <td>
                <a class="btn btn-xs btn-danger" href="delete.php?id=<?php echo $item['id'] ?>"<i class="fa fa-times"></i>Xóa</a>
            </td>
          </tr>
    <?php $stt++ ;endforeach ?>   
    </tbody>
  </table> 
    <div class="pull-right"></div>
    <nav aria-label="page navigation" class="clearfix">
      <ul class="pagination">
        <li>
          <a href=""  aria-label="previous">
            <span aria-hidden="true">&laquo;</span>
          </a>
        </li>
        <?php for(  $i=1;$i<=$sotrang;$i++) : ?>
          <?php 
            if(isset($_GET['page']))
            {
              $p=$_GET['page'];
            }
            else
            {
              $p=1;
            }
           ?>
           <li class="<?php echo($i==$p) ? 'active' : '' ?>">
                <a class="page-link" href="?page=<?php echo $i;?>"><?php echo $i; ?></a>
           </li>
         <?php endfor; ?>
         <li>
           <a href="" aria-label="Next">
             <span aria-hidden="true">&raquo;</span>
           </a>
         </li>
      </ul>
    </nav>
</div>  

<div class="dataTables_paginate paging_simple_numbers" id="dataTable_paginate">

</div>


  </div>
 
      </div>
      
  </div>
</div>





    <!-- /.content-wrapper -->



  <!-- /#wrapper -->
   <?php require_once __DIR__. "/../../layouts/header.php";?>