﻿<?php  
    require_once __DIR__."/autoload/autoload.php";
    if(isset($_SESSION['name_id']))
    {
        echo " <script>alert(' Bạn đã có tài khoản nên không thể vào ');location.href='index.php'</script>";
    }
    //xử lý
    $data =
    [
        "name" => postInput('name'),
        "email" =>postInput("email"),
        "phone"=>postInput("phone"),
        "password"=>md5(postInput("password")),
        "address"=>postInput("address")
    ]; 
    $error = [];   
    if($_SERVER["REQUEST_METHOD"] == "POST")
    {

        
        if($data['name']=='')
        {
            $error['name'] = "Tên không được để trống ! ";
        }
        
        if($data['email']=='')
        {
            $error['email'] = " Email không được để trống ! ";
        }
        else
        {
            $is_check = $db -> fetchOne("users"," email = '".$data['email']."' ");
            // _debug($is_check);

            if($is_check != NULL)
            {
                $error['email'] = "Email đã tồn tại nhập vào email khác";
            }
        }
        
        
        if($data['password']=='')
        {
            $error['password'] = "Password không được để trống ! ";
        }
        else
        {
            $data['password'] = md5(postInput("password"));
        }
       
        if($data['phone']=='')
        {
            $error['phone'] = "Phone không được để trống ! ";
        }
        
        if($data['address']=='')
        {
            $error['address'] = "Address không được để trống ! ";
        }
        if(empty($error))
        {
            $idinert = $db->insert("users",$data);
            if($idinert > 0)
            {
                $_SESSION['success']="Thêm mới thành công ! mời bạn đăng nhập";
               header("location:login.php");
            }
            else
            {

            }
        }
}

 ?>
    <?php  require_once __DIR__."/layouts/header.php" ;?>
        <div class="col-md-9 bor">
            <section class="box-main1">
                <h3 class="title-main"><a href=""> Đăng ký thánh viên </a> </h3>
                <form action="" method="POST" class="form-horizonal formcustome" role="form" style="margin-top:20px">
                    <div class="for-group">
                        <label class="col-md-2 col-md-offset-1">Tên thành viên</label>
                        <div class="col-md-8">
                            <input type="text" name="name" placeholder="Bui Dang Truong Kha" class="form-control">
                               <?php if (isset($error['name'])) : ?>
                                <p class="text-danger"><?php echo $error['name'] ?></p>
                                <?php endif ?>         
                        </div>  
                        
                    </div>

                    <div class="for-group">
                        <label class="col-md-2 col-md-offset-1">Email</label>
                        <div class="col-md-8">
                            <input type="email" name="email" placeholder="truongkha128@gmail.com" class="form-control" value="<?php echo $data['email'] ?>">
                                <?php if (isset($error['email'])) : ?>
                                <p class="text-danger"><?php echo $error['email'] ?></p>
                            <?php endif ?> 
                        </div> 
                            
                    </div>

                    <div class="for-group">
                        <label class="col-md-2 col-md-offset-1">Mật khẩu</label>
                        <div class="col-md-8">
                            <input type="password" name="password" placeholder="*******" class="form-control">
                            <?php if (isset($error['password'])) : ?>
                                <p class="text-danger"><?php echo $error['password'] ?></p>
                            <?php endif ?> 
                        </div>      
                    </div>

                    <div class="for-group">
                        <label class="col-md-2 col-md-offset-1">Số điện thoại</label>
                        <div class="col-md-8">
                            <input type="number" name="phone" placeholder="0976624657" class="form-control" value="<?php echo $data['phone'] ?>">
                            <?php if (isset($error['phone'])) : ?>
                                <p class="text-danger"><?php echo $error['phone'] ?></p>
                            <?php endif ?> 
                        </div>      
                    </div>

                    <div class="for-group">
                        <label class="col-md-2 col-md-offset-1">Địa chỉ</label>
                        <div class="col-md-8">
                            <input type="text" name="address" placeholder="Ktx khu B DHQG" class="form-control"value="<?php echo $data['address'] ?>">
                            <?php if (isset($error['address'])) : ?>
                                <p class="text-danger"><?php echo $error['address'] ?></p>
                            <?php endif ?> 
                        </div>      
                    </div>
                    <button type="submit" class="btn btn-success col-md-2 col-md-offset-5" style="margin-bottom: 20px">Đăng ký</button>
                </form>
            </section>
        </div>
    <?php  require_once __DIR__."/layouts/footer.php" ;?>
