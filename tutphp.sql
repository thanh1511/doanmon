-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th6 03, 2020 lúc 02:49 AM
-- Phiên bản máy phục vụ: 10.1.38-MariaDB
-- Phiên bản PHP: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `tutphp`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `level` tinyint(4) DEFAULT '1',
  `avatar` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `admin`
--

INSERT INTO `admin` (`id`, `name`, `address`, `email`, `password`, `phone`, `status`, `level`, `avatar`, `created_at`, `updated_at`) VALUES
(6, 'truongkha', 'KTX khu B, DHQG TpHCM', 'truongkha128@gmail.com', '588a56950bc2442b6223c97296f4a522', '0976624657', 1, 2, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `images` varchar(100) DEFAULT NULL,
  `banner` varchar(100) DEFAULT NULL,
  `home` tinyint(4) DEFAULT '0',
  `status` tinyint(4) DEFAULT '1',
  `created_ad` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_ad` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `category`
--

INSERT INTO `category` (`id`, `name`, `slug`, `images`, `banner`, `home`, `status`, `created_ad`, `update_ad`) VALUES
(27, 'Apple', 'apple', NULL, NULL, 1, 1, '2019-05-21 03:05:49', '2019-06-17 08:54:44'),
(28, 'Samsum Galaxy', 'samsum-galaxy', NULL, NULL, 1, 1, '2019-05-21 03:06:10', '2019-06-17 08:54:05'),
(29, 'Xiaomi', 'xiaomi', NULL, NULL, 1, 1, '2019-05-21 03:06:17', '2020-06-03 00:46:36'),
(31, 'Oppo', 'oppo', NULL, NULL, 1, 1, '2019-05-21 03:06:42', '2019-06-17 08:55:23');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `qty` tinyint(4) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `orders`
--

INSERT INTO `orders` (`id`, `transaction_id`, `product_id`, `qty`, `price`, `created_at`, `updated_at`) VALUES
(78, 44, 13, 1, 3450000, NULL, NULL),
(79, 45, 19, 2, 9500000, NULL, NULL),
(80, 46, 26, 2, 2100000, NULL, NULL),
(81, 47, 22, 2, 6790000, NULL, NULL),
(82, 48, 13, 1, 3450000, NULL, NULL),
(83, 49, 19, 2, 9500000, NULL, NULL),
(84, 50, 24, 1, 4020000, NULL, NULL),
(85, 51, 16, 1, 4700000, NULL, NULL),
(86, 52, 19, 1, 9500000, NULL, NULL),
(87, 53, 37, 1, 18000000, NULL, NULL),
(88, 54, 13, 2, 3450000, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `sale` tinyint(4) DEFAULT '0',
  `thunbar` varchar(100) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `content` text,
  `number` int(11) NOT NULL DEFAULT '0',
  `head` int(11) DEFAULT '0',
  `view` int(11) DEFAULT '0',
  `hot` tinyint(4) DEFAULT '0',
  `pay` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `product`
--

INSERT INTO `product` (`id`, `name`, `slug`, `price`, `sale`, `thunbar`, `category_id`, `content`, `number`, `head`, `view`, `hot`, `pay`, `created_at`, `updated_at`) VALUES
(15, 'Redmi Note 7 32GB', 'redmi-note-7-32gb', 3800000, 0, 'shopping.png', 29, 'bao hanh 2 nam', 2, 0, 0, 0, 4, NULL, '2019-06-18 02:17:59'),
(16, 'Điện Thoại OPPO F7 (128GB/6GB)', 'dien-thoai-oppo-f7-128gb6gb', 4700000, 0, 'shopping (1).png', 31, 'hàng chĩnh hãng bảo hành 12 tháng', 1, 0, 0, 0, 1, NULL, '2019-11-30 02:04:42'),
(18, 'iphone6s', 'iphone6s', 5600000, 0, 'iphone-6s-plus-silver-thumb_n8vx-y7.jpg', 27, 'hàng chĩnh hãng bảo hành 12 tháng', 2, 0, 0, 0, 3, NULL, '2019-06-18 02:18:06'),
(19, 'iphone8', 'iphone8', 10000000, 0, 'iphone-8-plus-xam-thumb_1_q2ic-ov.jpg', 27, 'hàng chĩnh hãng bảo hành 12 tháng', 7, 0, 0, 0, 4, NULL, '2019-12-20 02:52:42'),
(21, 'Samsung Galaxy M30', 'samsung-galaxy-m30', 2500000, 0, 'tải xuống.png', 28, 'bảo hành 8 tháng ', 1, 0, 0, 0, 2, NULL, '2019-06-18 02:19:08'),
(22, 'Samsung Galaxy S8 4GB/64GB', 'samsung-galaxy-s8-4gb64gb', 6790000, 0, 'tải xuống (1).png', 28, 'bảo hành 8 tháng', 3, 0, 0, 0, 3, NULL, '2019-12-20 02:52:53'),
(23, 'Xiaomi Redmi 6 4GB/64GB Đen', 'xiaomi-redmi-6-4gb64gb-den', 2780000, 0, 'shopping (2).png', 29, 'hàng chĩnh hãng bảo hành 12 tháng', 2, 0, 0, 0, 0, NULL, '2019-06-15 08:49:38'),
(24, 'Xiaomi Mi 8 Lite 64GB Ram 4GB', 'xiaomi-mi-8-lite-64gb-ram-4gb', 4020000, 0, 'shopping (3).png', 29, 'hàng chính hãng bảo hàng 12 tháng', 0, 0, 0, 0, 1, NULL, '2019-11-30 02:04:38'),
(25, 'OPPO A3s ', 'oppo-a3s', 3000000, 1, 'shopping (4).png', 31, 'hàng chính hãng bảo hành 12 tháng', 0, 0, 0, 0, 2, NULL, '2019-12-20 02:53:04'),
(26, 'Oppo F9', 'oppo-f9', 2100000, 0, 'shopping (5).png', 31, 'hàng chính hãng bảo hành 12 tháng', 3, 0, 0, 0, 2, NULL, '2019-12-20 02:53:14'),
(29, 'OPPO A3s a2', 'oppo-a3s-a2', 300000, 0, 'shopping (5).png', 31, 'bảo hành 12 tháng', 1, 0, 0, 0, 1, NULL, '2019-06-17 09:23:47'),
(30, 'Samsung Galaxy A7', 'samsung-galaxy-a7', 4000000, 0, 'shopping (2).png', 28, 'Bảo hành 12 tháng', 2, 0, 0, 0, 0, NULL, '2019-06-17 09:05:21'),
(32, 'Xiaomi redmi 7', 'xiaomi-redmi-7', 5000000, 0, 'allo.png', 29, 'Bảo hành 12 tháng', 2, 0, 0, 0, 0, NULL, '2019-06-17 09:09:43'),
(35, 'OPPO F1s', 'oppo-f1s', 5000000, 0, 'th.jpg', 31, 'hàng chất lượng tốt', 2, 0, 0, 0, 0, NULL, NULL),
(36, 'K20 spro', 'k20-spro', 8000000, 0, 'abcd.jpg', 29, 'Hàng rin chất lượng cao bảo hàng 1 năm', 2, 0, 0, 0, 0, NULL, '2019-12-20 03:01:37'),
(37, 'iphone 11', 'iphone-11', 18000000, 0, 'ihon.jpg', 27, 'Hảng quốc tế 128g bảo hành 1 năm', 0, 0, 0, 0, 1, NULL, '2019-12-20 07:04:28');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `transaction`
--

CREATE TABLE `transaction` (
  `id` int(11) NOT NULL,
  `amount` int(11) DEFAULT NULL,
  `users_id` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `note` text,
  `created_ad` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `transaction`
--

INSERT INTO `transaction` (`id`, `amount`, `users_id`, `status`, `note`, `created_ad`, `updated_at`) VALUES
(46, 4410000, 33, 1, 'giao hang tan noi', NULL, '2019-11-29 16:36:00'),
(47, 14259000, 33, 1, 'giao hang tan noi', NULL, '2019-11-29 16:40:58'),
(50, 4221000, 33, 1, 'giao hang tan noi', NULL, '2019-11-30 02:04:38'),
(51, 4935000, 34, 1, 'giao hang tan noi', NULL, '2019-11-30 02:04:42'),
(52, 9975000, 33, 1, 'giao hang tan noi', NULL, '2019-11-30 02:04:40'),
(54, 7245000, 35, 1, 'giao hang tan noi', NULL, '2019-12-20 07:23:30');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` char(15) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `token` varchar(50) DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `address`, `password`, `avatar`, `status`, `token`, `create_at`, `updated_at`) VALUES
(33, 'kha', 'truongkha128@gmail.com', '0976624657', 'KTX khu B, KHU PHỐ 6', '588a56950bc2442b6223c97296f4a522', NULL, 1, NULL, NULL, NULL),
(34, 'truongky', 'truongky26@gmail.com', '0976624657', 'KTX khu B, KHU PHỐ 6', '588a56950bc2442b6223c97296f4a522', NULL, 1, NULL, NULL, NULL),
(35, 'truonghai', 'truonghai85@gmail.com', '0976625768', 'Quan 1', '588a56950bc2442b6223c97296f4a522', NULL, 1, NULL, NULL, NULL),
(36, 'truonghai86', 'truonghai@gmail.com', '0976654546', 'KTX khu B, KHU PHỐ 6', '588a56950bc2442b6223c97296f4a522', NULL, 1, NULL, NULL, NULL);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cat` (`category_id`);

--
-- Chỉ mục cho bảng `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_id` (`users_id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT cho bảng `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT cho bảng `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT cho bảng `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
